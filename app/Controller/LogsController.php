<?php

App::uses('ApiController', 'Controller');

class LogsController extends AppController {
    
    public function index() 
    {
        
    }

    public function generateTrxId($length) {
        return substr(str_shuffle("0123456789"), 0, $length);
    }

    public function hit() 
    {
        $id = $this->request->data['Logs']['id_pelanggan'];

        $params['trx_date'] = date("YmdHis");
        $params['trx_id'] = $this->generateTrxId(10);
        $params['trx_type'] = '2100'; // 2100 = Inquiry, 2200 = Payment
        $params['cust_msisdn'] = '01428800711';
        $params['cust_account_no'] = $id;
        $params['product_id'] = '80'; // 80 = PLN Prepaid
        $params['product_nomination'] = ''; 
        // $params['product_nomination'] = '20000'; // use this line to makes payment request
        $params['periode_payment'] = '';
        $params['unsold'] = '';

        
        $input = json_encode($params, true);

        $request_date = date('Y-m-d H:i:s');
        $this->loadModel('Api');
        $output = $this->Api->hit($params);
        $response_date = date('Y-m-d H:i:s');

        $parse = json_decode($output, true);
        $data = $parse['data']['trx'];

        $response = $data['subscriber_id']." - ".$data['subscriber_name']." - ".$data['power'];

        $logs['product_code'] = $params['product_id'];
        $logs['customer_number'] = $params['cust_account_no'];
        $logs['trx_type'] = $params['trx_type'];
        $logs['request'] = $input;
        $logs['response'] = $response;
        $logs['request_date'] = $request_date;
        $logs['response_date'] = $response_date;    

        /**
         * save logs into database
         *
         */
        if ($this->request->is('post')) {
            $this->Log->create();

            if ($this->Log->save($logs)) {
                $this->Flash->success(__('Your post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }else {
                debug($out->errors()); die;
                $this->Flash->error(__('The out could not be saved. Please, try again.'));
            }

            $this->Flash->error(__('Unable to load your post.'));
        }

    }
}